---
date: 2019-11-19T01:42:45.000+00:00
hero_image: "/content/images/djim-loic-ft0-Xu4nTvA-unsplash.jpg"
title: Take the Flutter Clock Challenge and win a FREE iMac Pro
author: Jaydan Urwin

---
The Flutter team sent out this tweet a while ago with no details on what was coming and what it was for. After 12 days of anxiously waiting on what in the world this random Flutter announcement was for we finally know [it's a contest](https://flutter.dev/clock)!

So the Flutter team is putting on a contest in partnership with Lenovo and Google Assistant. The challenge is to _"Build a beautiful clock face UI with Flutter for the Lenovo Smart Clock for a chance to win an iMac Pro, Lenovo Smart Display, or Lenovo Smart Clock."_ I for one was really surprised by this and it gets me excited to see the Flutter team is finding creative ways to get the developer community involved in making real world products/features with Flutter.

### What do I win?

How about an iMac Pro worth $10,000?... haha ya I know I started working on my designs right after I read that 😂. The iMac isn't the only prize either. It looks like 4 winners will get [Lenovo Smart Displays](https://www.lenovo.com/us/en/smart-display) and up to 25 will get the [Lenovo Smart Clocks](https://www.lenovo.com/us/en/smart-clock) the contest is designed for.

### How do I enter?

Luckily it's pretty straight forward just follow the instructions on the [contest site](https://flutter.dev/clock)

I for one have started working on some concepts in a Figma file and started poking at the source code for their samples. I can't wait to what others create too so let's get out there and make something awesome!

May the best time-teller win! ⏱