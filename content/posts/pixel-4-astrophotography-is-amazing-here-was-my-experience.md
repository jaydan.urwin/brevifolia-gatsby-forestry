---
date: 2019-11-02T04:09:28Z
hero_image: "/content/images/IMG_20191026_233352_3.jpg"
title: Pixel 4 Astrophotography is AMAZING! Here was my experience
author: Jaydan Urwin

---
In case you haven't quite heard yet Google's new Pixel 4 and Pixel 4 XL, recently launched in October of this year, are able to capture photos of the stars. This unique niche of photography is largely known as astrophotography. Traditionally, astrophotography is a difficult skill that required you to have an expensive (multiple thousand dollar) DSLR or Mirrorless camera. Then, not only did you need the camera but you needed special lenses that weren't cheap, a decent tripod to hold it all still, and a lot of practice with long exposure photography to make it all work.

Now, all you need a smartphone and a tripod (and you don't even need that if there's a stump or rock nearby to hold it steady for 4 minutes). I already knew I was going to buy the Pixel 4 this year after all the rumors pointed to it being everything I hoped the Pixel 3 would be last year, to me the astrophotography was just icing on the cake. I've grown up in Idaho so, naturally, I've spent a lot of time in the mountains camping. One of our go-to places growing up was my grandparent's cabin not too far out of Idaho City in a little town called Placerville. I always remember hanging out by the fire late at night and looking up at the stars. It was so surreal to see them up there which is what made it the first place that came to mind to go try this thing out.

About a week after getting the phone my wife and I packed up the puppy and headed into the mountains for a night. Overall, the experience of using it was really straight forward. Here is everything I used:

1. [Pixel 4 XL](https://store.google.com/us/product/pixel_4)
2. [Tripod](https://www.bhphotovideo.com/c/product/842090-REG/magnus_vt_300_video_tripod_w_2_way.html)
3. [Beastgrip Pro](https://beastgrip.com/collections/shop-now/products/beastgrip-pro)

Once I got the phone mounted to the tripod I just angled the phone upwards, set the phone's camera to Night Sight, and tapped the shutter. The photo had a timer on it for 4 minutes and once it finished it processed it in about 2 seconds and it was done. That's it! That's all I did and I think the results speak for themselves. You can find the full album of photos [here](https://photos.app.goo.gl/26Pz34PRvYhFWmQg8) if you'd like to download and take a look.